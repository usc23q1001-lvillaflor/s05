from abc import ABC, abstractmethod

class Animal(ABC):
	@abstractmethod
	def eat(food):
		pass

	@abstractmethod
	def make_sound():
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

	def get_name(self):
		print(f'Name: {self._name}')

	def get_breed(self):
		print(f'Breed: {self._breed}')

	def get_age(self):
		print(f'Age: {self._age}')

	def eat(self, food):
		print(f'Serve me {food}')

	def make_sound(self):
		print(f'Miaow! Nyaw! Nyaaaaa!')

	def call(self):
		print(f'{self._name}, come on!')

class Dog(Animal):
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

	def get_name(self):
		print(f'Name: {self._name}')

	def get_breed(self):
		print(f'Breed: {self._breed}')

	def get_age(self):
		print(f'Age: {self._age}')

	def eat(self, food):
		print(f'Eaten {food}')

	def make_sound(self):
		print(f'Bark! Woof! Arf!')

	def call(self):
		print(f'Here {self._name}!')

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()