class Person():
	def __init__(self, name, age):
		self.name = name
		self.age = age

	def set_name(self, name):
		self.name = name

	def set_age(self, age):
		self.age = age

	def get_name(self):
		print(f'Name of person: {self.name}')

	def get_age(self):
		print(f'Age of person: {self.age}')


p1 = Person("Bob",20)
p2 = Person("Jared", 22)

p1.get_name()
p1.get_age()
p2.get_name()
p2.get_age()

class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__("Bob", 20)
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	def set_student_no(self, student_no):
		self._student_no = student_no

	def set_course(self, course):
		self._course = course

	def set_year_level(self, year_level):
		self._year_level = year_level

	def get_student_no(self):
		print(f'Student number: {self._student_no}')

	def get_course(self):
		print(f'Course: {self._course}')

	def get_year_level(self):
		print(f'Year level: {self._year_level}')

	def get_details(self):
		print(f'{self.name} is currently in year {self._year_level} taking up {self._course}')

p3 = Student(19101152,"BSCS",4)
p3.get_details()

def test_function(obj):
	obj.is_admin()
	obj.usertype()